package com.example.restapimybatishw002.service.serviceImplement;

import com.example.restapimybatishw002.model.entity.Customer;
import com.example.restapimybatishw002.model.request.CustomerRequest;
import com.example.restapimybatishw002.repository.CustomerRepository;
import com.example.restapimybatishw002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImplement implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImplement(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.addNewCustomer(customerRequest);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer cusId) {
        return customerRepository.getCustomerById(cusId);
    }

    @Override
    public boolean deleteCustomerById(Integer cusId) {
        return customerRepository.deleteCustomerById(cusId);
    }

    @Override
    public Integer updateCustomerById(CustomerRequest customerRequest, Integer cusId) {
        return customerRepository.updateCustomerById(customerRequest, cusId);
    }

}
