package com.example.restapimybatishw002.controller;

import com.example.restapimybatishw002.model.entity.Customer;
import com.example.restapimybatishw002.model.request.CustomerRequest;
import com.example.restapimybatishw002.model.response.CustomerResponse;
import com.example.restapimybatishw002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/api/v1/add-new-customer")
    @Operation(summary = "Add new customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest) {
        Integer storeCustomer = customerService.addNewCustomer(customerRequest);
        if (storeCustomer != null) {
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Insert successfully")
                    .payload(customerService.getCustomerById(storeCustomer))
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    //get all customer
    @GetMapping("/api/v1/get-all-customer")
    @Operation(summary = "Get all Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer() {
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Get all customer successfully")
                .payload(customerService.getAllCustomers())
                .httpStatus(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(response);
    }


    //get customer by id
    @GetMapping("/api/v1/get-customer-by-id/{id}")
    @Operation(summary = "Get Customer by id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer cusId) {
        CustomerResponse<Customer> response = null;
        if (customerService.getCustomerById(cusId) != null) {
            response = CustomerResponse.<Customer> builder()
                    .payload(customerService.getCustomerById(cusId))
                    .message("Get Customer by id successfully")
                    .httpStatus(HttpStatus.OK)
                    .build();
        }
            return ResponseEntity.ok(response);
    }

    //delete customer by id
    @DeleteMapping("/api/v1/delete-customer-by-id/{id}")
    @Operation(summary = "Delete Customer by id")
    public ResponseEntity<CustomerResponse<String>> deleteAuthorById(@PathVariable("id") Integer cusId) {
        CustomerResponse<String> response = null;
        if (customerService.deleteCustomerById(cusId)) {
            response = CustomerResponse.<String>builder()
                    .message("Deleted successfully")
                    .httpStatus(HttpStatus.OK)
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    //Update customer by id
    @PutMapping("/api/v1/update-customer-by-id/{id}")
    @Operation(summary = "Update Author by id")
    public ResponseEntity<CustomerResponse<Customer>> updateAuthorById(
            @RequestBody CustomerRequest authorRequest, @PathVariable("id") Integer authorId
    ) {
        CustomerResponse<Customer> response = null;
        Integer idUpdateAuthor = customerService.updateCustomerById(authorRequest, authorId);
        if (idUpdateAuthor != null) {
            response = CustomerResponse.<Customer>builder()
                    .message("Updated successfully")
                    .payload(customerService.getCustomerById(idUpdateAuthor))
                    .httpStatus(HttpStatus.OK)
                    .build();
        }
        return ResponseEntity.ok(response);

    }

}
