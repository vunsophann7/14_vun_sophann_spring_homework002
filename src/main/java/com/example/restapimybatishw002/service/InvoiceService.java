package com.example.restapimybatishw002.service;

import com.example.restapimybatishw002.model.entity.Invoice;
import com.example.restapimybatishw002.model.request.InvoiceRequest;
import org.springframework.stereotype.Service;

import java.util.List;

public interface InvoiceService {
    //get all invoices
    List<Invoice> getAllInvoices();
    //get invoice by id
    Invoice getInvoiceById(Integer invoiceId);
    //delete invoice by id
    boolean deleteInvoiceById(Integer invoiceId);

}
