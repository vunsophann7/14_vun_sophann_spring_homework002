package com.example.restapimybatishw002.service.serviceImplement;

import com.example.restapimybatishw002.model.entity.Product;
import com.example.restapimybatishw002.model.request.ProductRequest;
import com.example.restapimybatishw002.repository.ProductRepository;
import com.example.restapimybatishw002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImplement implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImplement(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProducts();
    }

    @Override
    public Product getProductById(Integer proId) {
        return productRepository.getProductById(proId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        return productRepository.addNewProduct(productRequest);
    }

    @Override
    public Integer updateProductById(ProductRequest productRequest, Integer proId) {
        return productRepository.updateProductById(productRequest, proId);
    }

    @Override
    public boolean deleteProductById(Integer proId) {
        return productRepository.deleteProductById(proId);
    }
}
