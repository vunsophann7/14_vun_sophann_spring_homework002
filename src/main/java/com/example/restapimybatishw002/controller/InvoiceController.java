package com.example.restapimybatishw002.controller;


import com.example.restapimybatishw002.model.entity.Invoice;
import com.example.restapimybatishw002.model.response.InvoiceResponse;
import com.example.restapimybatishw002.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InvoiceController {
    private final InvoiceService invoiceService;


    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    @GetMapping("/api/v1/get-all-invoice")
    @Operation(summary = "Get all Invoices")
    public ResponseEntity<List<Invoice>> getAllInvoices() {
        return ResponseEntity.ok(invoiceService.getAllInvoices());
    }

    @GetMapping("/api/v1/get-invoice-by-id/{id}")
    @Operation(summary = "Get invoice by id")
    public ResponseEntity<Invoice> getInvoiceById(@PathVariable("id") Integer invoiceId) {
        return ResponseEntity.ok(invoiceService.getInvoiceById(invoiceId));
    }

    @DeleteMapping("/api/v1/delete-invoice-by-id/{id}")
    @Operation(summary = "Delete Invoice by id")
    public ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(@PathVariable("id") Integer invoiceId) {
        InvoiceResponse<String> response = null;
        if (invoiceService.deleteInvoiceById(invoiceId)) {
            response = InvoiceResponse.<String>builder()
                    .message("Deleted successfully")
                    .status(HttpStatus.OK)
                    .build();
        }
        return ResponseEntity.ok(response);
    }




}
