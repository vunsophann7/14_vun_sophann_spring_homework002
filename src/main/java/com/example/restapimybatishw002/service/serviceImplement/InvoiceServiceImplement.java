package com.example.restapimybatishw002.service.serviceImplement;

import com.example.restapimybatishw002.model.entity.Invoice;
import com.example.restapimybatishw002.repository.InvoiceRepository;
import com.example.restapimybatishw002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImplement implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImplement(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoices() {
        return invoiceRepository.findAllInvoices();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }

}
