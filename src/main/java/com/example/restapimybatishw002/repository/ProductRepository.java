package com.example.restapimybatishw002.repository;

import com.example.restapimybatishw002.model.entity.Product;
import com.example.restapimybatishw002.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT pro.id, pro.name, pro.price FROM invoice_detail in_d " +
            "INNER JOIN products pro ON pro.id = in_d.product_id " +
            "WHERE in_d.invoice_id = #{invoiceId}")
    List<Product> getProductByInvoiceId(Integer invoiceId);

    //add new product
    @Select("INSERT INTO products (name, price) VALUES(#{request.name}, #{request.price}) RETURNING id")
    Integer addNewProduct(@Param("request")ProductRequest productRequest);

    //get all product
    @Select("SELECT * FROM products")
    List<Product> findAllProducts();

    //get product by id
    @Select("SELECT * FROM products WHERE id = #{proId}")
    Product getProductById(Integer proId);

    //update product by id
    @Select("UPDATE products SET name=#{request.name}, price=#{request.price} WHERE id = #{proId} RETURNING id")
    Integer updateProductById(@Param("request") ProductRequest productRequest, Integer proId);

    //delete product by id
    @Delete("DELETE FROM products WHERE id = #{id}")
    boolean deleteProductById(@Param("id") Integer cusId);

}
