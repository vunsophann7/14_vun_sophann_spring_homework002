package com.example.restapimybatishw002.service;

import com.example.restapimybatishw002.model.entity.Product;
import com.example.restapimybatishw002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    //get product by id
    Product getProductById(Integer proId);

    //add new product
    Integer addNewProduct(ProductRequest productRequest);

    //update product by id
    Integer updateProductById(ProductRequest productRequest, Integer proId);

    //delete product by id
    boolean deleteProductById(Integer proId);

}
