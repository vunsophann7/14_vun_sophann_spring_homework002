package com.example.restapimybatishw002.service;

import com.example.restapimybatishw002.model.entity.Customer;
import com.example.restapimybatishw002.model.request.CustomerRequest;

import java.util.List;


public interface CustomerService {
    //add new customer
    Integer addNewCustomer(CustomerRequest customerRequest);

    //get all customer
    List<Customer> getAllCustomers();

    //get customer by id
    Customer getCustomerById(Integer cusId);

    //delete customer by id
    boolean deleteCustomerById(Integer cusId);

    //update customer by id
    Integer updateCustomerById(CustomerRequest customerRequest, Integer cusId);
}
