package com.example.restapimybatishw002.repository;

import com.example.restapimybatishw002.model.entity.Invoice;
import com.example.restapimybatishw002.model.request.InvoiceRequest;
import com.example.restapimybatishw002.service.InvoiceService;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoices")
    @Result(property = "id", column = "id")
    @Results(
            id="invoiceMapper",
            value = {
                    @Result(property = "timestamp", column = "date"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select = "com.example.restapimybatishw002.repository.CustomerRepository.getCustomerById")
                    ),
                    @Result(property = "products", column = "id",
                            many = @Many(select = "com.example.restapimybatishw002.repository.ProductRepository.getProductByInvoiceId")
                    )
            }
    )

    List<Invoice> findAllInvoices();

    //get invoice by id
    @Select("SELECT * FROM invoices WHERE id = #{invoiceId}")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById(Integer invoiceId);

    //delete invoice by id
    @Delete("DELETE FROM invoices WHERE id = #{id}")
    boolean deleteInvoiceById(@Param("id") Integer invoiceId);


}
