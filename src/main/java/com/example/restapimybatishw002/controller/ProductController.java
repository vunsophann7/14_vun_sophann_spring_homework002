package com.example.restapimybatishw002.controller;

import com.example.restapimybatishw002.model.entity.Customer;
import com.example.restapimybatishw002.model.entity.Product;
import com.example.restapimybatishw002.model.request.CustomerRequest;
import com.example.restapimybatishw002.model.request.ProductRequest;
import com.example.restapimybatishw002.model.response.CustomerResponse;
import com.example.restapimybatishw002.model.response.ProductResponse;
import com.example.restapimybatishw002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/api/v1/get-all-products")
    @Operation(summary = "Get all Product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProducts() {
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProducts())
                .message("Get all product successfully")
                .success(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/v1/get-product-by-id/{id}")
    @Operation(summary = "Get Product by id")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer proId) {
        ProductResponse<Product> response = null;
        if (productService.getProductById(proId) != null) {
            response = ProductResponse.<Product> builder()
                    .message("Get Product by id successfully")
                    .payload(productService.getProductById(proId))
                    .success(HttpStatus.OK)
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/api/v1/add-new-product")
    @Operation(summary = "Add new product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest) {
        Integer storeProduct = productService.addNewProduct(productRequest);
        if (storeProduct != null) {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(storeProduct))
                    .message("Add new successfully")
                    .success(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


    //Update product by id
    @PutMapping("/api/v1/update-product-by-id/{id}")
    @Operation(summary = "Update product by id")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest, @PathVariable("id") Integer proId
    ) {
        ProductResponse<Product> response = null;
        Integer idProductUpdate = productService.updateProductById(productRequest, proId);
        if (idProductUpdate != null) {
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(idProductUpdate))
                    .message("Updated successfully")
                    .success(HttpStatus.OK)
                    .build();
        }
        return ResponseEntity.ok(response);

    }

    //delete product by id
    @DeleteMapping("/api/v1/delete-product-by-id/{id}")
    @Operation(summary = "Delete product by id")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer proId) {
        ProductResponse<String> response = null;
        if (productService.deleteProductById(proId)) {
            response = ProductResponse.<String>builder()
                    .message("Deleted successfully")
                    .success(HttpStatus.OK)
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}
