package com.example.restapimybatishw002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiMyBatisHw002Application {

    public static void main(String[] args) {
        SpringApplication.run(RestApiMyBatisHw002Application.class, args);
    }

}
