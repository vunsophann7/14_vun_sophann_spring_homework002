CREATE TABLE customers(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(255),
    address VARCHAR(255),
    phone VARCHAR(255)
);
CREATE TABLE invoices(
    id SERIAL PRIMARY KEY ,
    date TIMESTAMP,
    customer_id INT REFERENCES customers(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE products(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(255) NOT NULL ,
    price INT
);
CREATE TABLE invoice_detail(
    invoice_id INT REFERENCES invoices(id) ON DELETE CASCADE ON UPDATE CASCADE ,
    product_id INT REFERENCES products(id) ON DELETE CASCADE ON UPDATE CASCADE ,
    PRIMARY KEY (invoice_id, product_id)
);