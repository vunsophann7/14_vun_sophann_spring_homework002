package com.example.restapimybatishw002.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceResponse<T> {
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private HttpStatus status;

}
