package com.example.restapimybatishw002.repository;

import com.example.restapimybatishw002.model.entity.Customer;
import com.example.restapimybatishw002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    //add new customer
    @Select("INSERT INTO customers (name, address, phone) " +
            "VALUES(#{request.name}, #{request.address}, #{request.phone})" + "RETURNING id")
    Integer addNewCustomer(@Param("request")CustomerRequest customerRequest);

    //get all customer
    @Select("SELECT * FROM customers")
    List<Customer> findAllCustomer();

    //get customer by id
    @Select("SELECT * FROM customers WHERE id = #{cusId}")
    Customer getCustomerById(Integer cusId);

    //delete customer by id
    @Delete("DELETE FROM customers WHERE id = #{id}")
    boolean deleteCustomerById(@Param("id") Integer cusId);

    //update customer by id
    @Update("UPDATE customers " +
            "SET name = #{request.name}," +
            "address = #{request.address}, " +
            "phone = #{request.phone} " +
            "WHERE id = #{cusId} " +
            "RETURNING id")
    Integer updateCustomerById(@Param("request") CustomerRequest customerRequest, Integer cusId);

}
